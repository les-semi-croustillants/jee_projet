-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  ven. 23 nov. 2018 à 09:29
-- Version du serveur :  5.7.19
-- Version de PHP :  7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `projet`
--

-- --------------------------------------------------------

--
-- Structure de la table `employes`
--

DROP TABLE IF EXISTS `employes`;
CREATE TABLE IF NOT EXISTS `employes` (
  `ID` int(12) NOT NULL AUTO_INCREMENT,
  `NOM` varchar(50) NOT NULL,
  `PRENOM` varchar(50) NOT NULL,
  `TELDOM` varchar(10) NOT NULL,
  `TELPORT` varchar(10) NOT NULL,
  `TELPRO` varchar(10) NOT NULL,
  `ADRESSE` varchar(200) NOT NULL,
  `VILLE` varchar(50) NOT NULL,
  `CP` int(5) NOT NULL,
  `EMAIL` varchar(100) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='EMPLOYE table';

--
-- Déchargement des données de la table `employes`
--

INSERT INTO `Projet`.`employes` (`ID`, `NOM`, `PRENOM`, `TELDOM`, `TELPORT`, `TELPRO`, `ADRESSE`, `VILLE`, `CP`, `EMAIL`) VALUES
(1, 'Turing', 'Alan', '0123456789', '0612345678', '0698765432', '2 rue des Automates', 'Colombes', 92700, 'aturing@efrei.fr'),
(2, 'Galois', 'Evariste', '0145362787', '0645362718', '0611563477', '21 rue des Morts-trop-jeunes', 'Bois-colombes', 92270, 'egalois@efrei.fr'),
(3, 'Boole', 'George', '0187665987', '0623334256', '0654778654', '65 rue de la Logique', 'Colombes', 92700, 'gboole@efrei.fr'),
(4, 'Gauss', 'Carl Friedrich', '0187611987', '0783334256', '0658878654', '6 rue des Transformations', 'Paris', 75016, 'cgauss@efrei.fr'),
(5, 'Pascal', 'Blaise', '0187384987', '0622494256', '0674178654', '39 bvd de Port-Royal', 'Dijon', 21000, 'bpascal@efrei.fr'),
(6, 'Euler', 'Leonhard', '0122456678', '0699854673', '0623445166', '140 avenue Complexe', 'Nanterre', 90000, 'leuler@efrei.fr'),
(7, 'Jean', 'Pierre', '0122456678', '0699854673', '0623445166', '14 avenue Complexe', 'Nanterre', 90000, 'jp@efrei.fr'),
(8, 'Marie', 'Jeanne', '0122456679', '0699854672', '0623445167', '13 avenue Complexe', 'Nanterre', 90000, 'mj@efrei.fr'),
(9, 'Paul', 'Leonhard', '0122456690', '0699854674', '0623445169', '12 avenue Complexe', 'Nanterre', 90000, 'pl@efrei.fr'),
(10, 'Euler', 'Paul', '0122456638', '0699854675', '0623445161', '12 avenue Complexe', 'Nanterre', 90000, 'eup@efrei.fr');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

DROP TABLE IF EXISTS `Projet`.`users`;
CREATE TABLE IF NOT EXISTS `Projet`.`users` (
  `LOGIN` varchar(50) NOT NULL,
  `MDP` varchar(50) NOT NULL,
  PRIMARY KEY (`LOGIN`)
);

INSERT INTO `Projet`.`users` (`LOGIN`, `MDP`) VALUES
('adm', 'adm');
COMMIT;
