<%-- 
    Document   : addemploye
    Created on : 26 nov. 2018, 13:32:43
    Author     : jbi
--%>

<%@page import="pojo.Employe"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
       <link href="ressources/css/header.css" rel="stylesheet">
       <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
        <title>Semi-Croustillant</title>
    </head>
    <body>
        <jsp:include page="header.jsp"/>
        <h1>Ajout d'un membre </h1></br>
        <div style="margin-left: 20px;">
            <form action="Controler" method = "POST" style="width: 80%; height: 60%;">
                <div class="form-row">
                <label for='prenom'>Prénom : </label>
                    <input type='text' name='prenom' class="form-control"/>
                    </div>        
                    <div class="form-row">
                <label for='nom'>Nom : </label>
                    <input type='text' name='nom'class="form-control"/>
                    </div>        

                    <div class="form-row">
                    <label for='telDom'>Tél Domicile : </label>
                    <input type='text' name='telDom' class="form-control"/>
                    </div>

                    <div class="form-row">	
                    <label for='telMob'>Tél Mobile : </label>
                    <input type='text' name='telMob' class="form-control"/>
                    </div>

                    <div class="form-row">	
                    <label for='telPro'>Tél Pro : </label>
                    <input type='text' name='telPro' class="form-control"/>
                    </div>

                    <div class="form-row">
                            <div class="form-group col-md-6">
                                    <label for='adresse'>Adresse : </label>
                                    <input type='text' name='adresse' class="form-control"/>
                            </div>
                            <div class="form-group col-md-6">
                                    <label for='cp'>Code postale : </label>
                                    <input type='text' name='cp' class="form-control"/>

                            </div>
                    </div>	
                    <div class="form-row">
                            <div class="form-group col-md-6">
                                    <label for='ville'>Ville : </label>
                                    <input type='text' name='ville'  class="form-control"/>
                            </div>
                            <div class="form-group col-md-6">
                                    <label for='mail'>Adresse e-mail : </label>
                                    <input type='text' name='mail'  class="form-control"/>
                            </div>

                    </div>
                <button type="submit" name="action" value="Add" class="btn btn-primary">Ajouter</button>
                <button type="submit" name="action" value="EmployeAction"class="btn btn-primary">Voir liste</button>
            </form>
        </div>    
    </body>
</html>
