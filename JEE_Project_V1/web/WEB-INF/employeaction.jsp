<%@page import="java.util.List"%>
<%@page import="pojo.Employe"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
       <link href="ressources/css/header.css" rel="stylesheet">
       <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
        <title>Semi-Croustillant</title>
    </head>
    <body>
        
        <jsp:include page="header.jsp"/>
        <br>
        <h1>Liste des employ�s </h1>
        <form action="Controler" method = "POST">
            <table class="table">
                <thead class="thead-dark">
                <tr>
                    <th scope="col">S�l</th>
                    <th scope="col">Nom</th>
                    <th scope="col">Prenom</th>
                    <th scope="col">TelDom</th>
                    <th scope="col">TelPor</th>
                    <th scope="col">TelPro</th>
                    <th scope="col">Adresse</th>
                    <th scope="col">CP</th>
                    <th scope="col">Ville</th>
                    <th scope="col">Email</th>
                </tr>
                </thead>
                <tbody>
                <%
                    List<Employe> userList = (List<Employe>)session.getAttribute("listUser");
                    if(userList.isEmpty()){
                        out.print("<td>" + session.getAttribute("empty_list_message") + "</td>");
                    } else {
                    for(Employe employe : userList){
                        out.print("<tr><td>" + "<input type='radio' name='employeId' value='"+ employe.getId() +"'>" + "</td>");
                        out.print("<td>" + employe.getNom() + "</td>");
                        out.print("<td>" + employe.getPrenom() + "</td>");
                        out.print("<td>" + employe.getTeldom() + "</td>");
                        out.print("<td>" + employe.getTelport()+ "</td>");
                        out.print("<td>" + employe.getTelpro() + "</td>");
                        out.print("<td>" + employe.getAdresse() + "</td>");
                        out.print("<td>" + employe.getCodepostal() + "</td>");
                        out.print("<td>" + employe.getVille() + "</td>");
                        out.print("<td>" + employe.getEmail() + "</td></tr>");
                    }
                    }
                %>
                </tbody>
            </table>
            <button class="btn btn-primary" type="submit" name="action" value="Delete">Supprimer</button>
            <button class="btn btn-primary" type="submit" name="action" value="Details">D�tails</button>
            <button class="btn btn-light" type="submit" name="action" value="Create">Ajouter</button>
        </form>
                <p style="color: red"><%= session.getAttribute("error_message") %></p>  
                <p style="color: blue"><%= session.getAttribute("success_message") %></p>    
    </body>
</html>
