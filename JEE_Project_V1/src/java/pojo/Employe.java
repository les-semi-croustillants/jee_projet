/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pojo;

/**
 *
 * @author jbi
 */
public class Employe {
    private Integer id;
    private String nom;
    private String prenom;
    private String teldom;
    private String telport;
    private String telpro;
    private String adresse;
    private String codepostal;
    private String ville;
    private String email;

    @Override
    public String toString() {
        return "Employe{" + "id=" + id + ", nom=" + nom + ", prenom=" + prenom + ", teldom=" + teldom + ", telport=" + telport + ", telpro=" + telpro + ", adresse=" + adresse + ", codepostal=" + codepostal + ", ville=" + ville + ", email=" + email + '}';
    }
    
    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the nom
     */
    public String getNom() {
        return nom;
    }

    /**
     * @param nom the nom to set
     */
    public void setNom(String nom) {
        this.nom = nom;
    }

    /**
     * @return the prenom
     */
    public String getPrenom() {
        return prenom;
    }

    /**
     * @param prenom the prenom to set
     */
    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    /**
     * @return the teldom
     */
    public String getTeldom() {
        return teldom;
    }

    /**
     * @param teldom the teldom to set
     */
    public void setTeldom(String teldom) {
        this.teldom = teldom;
    }

    /**
     * @return the telport
     */
    public String getTelport() {
        return telport;
    }

    /**
     * @param telport the telport to set
     */
    public void setTelport(String telport) {
        this.telport = telport;
    }

    /**
     * @return the telpro
     */
    public String getTelpro() {
        return telpro;
    }

    /**
     * @param telpro the telpro to set
     */
    public void setTelpro(String telpro) {
        this.telpro = telpro;
    }

    /**
     * @return the adresse
     */
    public String getAdresse() {
        return adresse;
    }

    /**
     * @param adresse the adresse to set
     */
    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    /**
     * @return the codepostal
     */
    public String getCodepostal() {
        return codepostal;
    }

    /**
     * @param codepostal the codepostal to set
     */
    public void setCodepostal(String codepostal) {
        this.codepostal = codepostal;
    }

    /**
     * @return the ville
     */
    public String getVille() {
        return ville;
    }

    /**
     * @param ville the ville to set
     */
    public void setVille(String ville) {
        this.ville = ville;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }
}
