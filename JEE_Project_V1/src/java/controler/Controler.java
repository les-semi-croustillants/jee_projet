/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controler;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import pojo.Employe;
import util.CheckUtil;
import util.Credentials;
import util.DataAccess;
import util.PropertiesUtil;
import util.constants.MessageConstantes;
import util.constants.URIConstantes;


/**
 * 
 * @author hp
 */
public class Controler extends HttpServlet {

    private List<Employe> listUser;
    
    private Employe selectedEmploye;
    
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            String action = request.getParameter("action");
            resetStringSessionAttribute(request.getSession(true), "error_message");
            resetStringSessionAttribute(request.getSession(true), "empty_list_message");
            resetStringSessionAttribute(request.getSession(true), "success_message");
            boolean isSessionActive = request.getSession(true).getAttribute("session") != null;
            if(action == null){
                loginActionProcessRequest(request, response);
            } else {
                switch(action){
                    case "Login":
                        String login = request.getParameter("login");
                        String mdp = request.getParameter("password");
                        
                        if(!CheckUtil.isEmptyString(login) && !CheckUtil.isEmptyString(mdp)){
                            if(checkCredentials(login, mdp)){
                                request.getSession(true).setAttribute("session", true);
                                employeActionProcessRequest(request, response);
                            } else {
                                request.getSession().setAttribute("error_message",PropertiesUtil.getErrorMessageProperty(MessageConstantes.AUTHENTIFICATION_ERROR_MESSAGE));
                                loginActionProcessRequest(request, response);
                            }
                        } else {
                            request.getSession().setAttribute("error_message",PropertiesUtil.getErrorMessageProperty(MessageConstantes.MISSING_FIELD_ERROR_MESSAGE));
                            loginActionProcessRequest(request, response);
                        }
                        break;
                    case "EmployeAction" :
                        if(isSessionActive){
                            employeActionProcessRequest(request, response);
                        } else {
                            loginActionProcessRequest(request, response);
                        }
                        break;
                        
                    case "Create" :
                        if(isSessionActive){
                            createActionProcessRequest(request, response);
                        } else {
                            loginActionProcessRequest(request, response);
                        }
                        break;
                        
                    case "Add":
                        if(isSessionActive){
                            addEmployeActionProcessRequest(request, response);
                        } else {
                            
                        }
                        break;
                        
                    case "Details":
                        if(isSessionActive){
                            detailsActionProcessRequest(request, response);
                        } else {
                            loginActionProcessRequest(request, response);
                        }
                        break;
                        
                    case "Update":
                        if(isSessionActive){
                            updateEmployeActionProcessRequest(request, response);
                        } else {
                            loginActionProcessRequest(request, response);
                        }
                        break;

                    case "Delete":
                        if(isSessionActive){
                            deleteEmployeActionProcessRequest(request, response);
                        } else {
                            loginActionProcessRequest(request, response);
                        }
                        break;
                        
                    default :
                        loginActionProcessRequest(request, response);
                        break;
                }
            }        
        }
    }

    
    /**
     * 
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException 
     */
    private void loginActionProcessRequest(HttpServletRequest request, HttpServletResponse response) 
            throws ServletException, IOException {
        request.getRequestDispatcher(PropertiesUtil.getURIProperty(URIConstantes.LOGIN_URI)).forward(request, response); //"WEB-INF/login.jsp"
    }

    /**
     * 
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException 
     */
    private void employeActionProcessRequest(HttpServletRequest request, HttpServletResponse response) 
            throws ServletException, IOException {
        HttpSession session = request.getSession(true);
        
        DataAccess con = new DataAccess();

        initUserList(con);
        if(CheckUtil.isEmptyList(listUser)){
            session.setAttribute("empty_list_message", PropertiesUtil.getErrorMessageProperty(MessageConstantes.EMPTY_LIST_ERROR_MESSAGE));
        }
        session.setAttribute("listUser", listUser);
        session.setAttribute("selectedEmploye", null);
        session.setAttribute("employeId", null);
        request.getRequestDispatcher(PropertiesUtil.getURIProperty(URIConstantes.EMPLOYE_ACTION_URI)).forward(request, response); //"WEB-INF/employeaction.jsp"
    }
    
    /**
     * 
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException 
     */
    private void createActionProcessRequest(HttpServletRequest request, HttpServletResponse response) 
            throws ServletException, IOException {
        request.getRequestDispatcher(PropertiesUtil.getURIProperty(URIConstantes.ADD_EMPLOYE_URI)).forward(request, response); 
    }
    
    private void addEmployeActionProcessRequest(HttpServletRequest request, HttpServletResponse response) 
            throws ServletException, IOException {
        DataAccess con = new DataAccess();
        Employe addEmploye = getEmployeFromRequest(request);
        con.addEmploye(addEmploye);
        employeActionProcessRequest(request, response);
    }
    /**
     * 
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException 
     */
    private void detailsActionProcessRequest(HttpServletRequest request, HttpServletResponse response) 
            throws ServletException, IOException {
        try{
            Integer id = Integer.parseInt(request.getParameter("employeId"));
            selectedEmploye = getEmployeFromId(id);
            request.getSession(true).setAttribute("selectedEmploye", selectedEmploye);
            request.getRequestDispatcher(PropertiesUtil.getURIProperty(URIConstantes.DETAILS_URI)).forward(request, response); 
        } catch(NumberFormatException e) {
            request.getSession().setAttribute("error_message",PropertiesUtil.getErrorMessageProperty(MessageConstantes.MANDATORY_SELECTION_MESSAGE));
            employeActionProcessRequest(request, response);
        }
    }
    /**
     * 
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException 
     */
    private void updateEmployeActionProcessRequest(HttpServletRequest request, HttpServletResponse response) 
            throws ServletException, IOException {
        Employe updateEmploye = getEmployeFromRequest(request);
        updateEmploye.setId(Integer.parseInt(request.getParameter("id")));
        DataAccess con = new DataAccess();
        con.updateEmploye(updateEmploye);
        employeActionProcessRequest(request, response);
    }
    /**
     * 
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException 
     */
    private void deleteEmployeActionProcessRequest(HttpServletRequest request, HttpServletResponse response) 
            throws ServletException, IOException {
        try{
            DataAccess con = new DataAccess();
            Integer id = Integer.parseInt(request.getParameter("employeId"));
            con.deleteEmploye(id);  
            request.getSession().setAttribute("success_message",PropertiesUtil.getErrorMessageProperty(MessageConstantes.DELETE_SUCCESS_MESSAGE));
            employeActionProcessRequest(request, response);
        } catch(NumberFormatException e) {
            request.getSession().setAttribute("error_message",PropertiesUtil.getErrorMessageProperty(MessageConstantes.MANDATORY_SELECTION_MESSAGE));
            employeActionProcessRequest(request, response);
        }
    }
    
    /**
     * 
     * @param session
     * @param error_message 
     */
    private void resetStringSessionAttribute(HttpSession session, String error_message) {
        session.setAttribute(error_message, "");
    }
    
    /**
     * 
     * @param id
     * @return 
     */
    private Employe getEmployeFromId(Integer id){
        return listUser.stream().filter(x -> x.getId().equals(id)).findAny().get();
    }
    
    /**
     * 
     * @param request
     * @return 
     */
    private Employe getEmployeFromRequest(HttpServletRequest request) {
        Employe employe = new Employe();
        employe.setAdresse(request.getParameter("adresse"));
        employe.setCodepostal(request.getParameter("cp"));
        employe.setEmail(request.getParameter("mail"));
        employe.setNom(request.getParameter("nom"));
        employe.setPrenom(request.getParameter("prenom"));
        employe.setTeldom(request.getParameter("telDom"));
        employe.setTelport(request.getParameter("telMob"));
        employe.setTelpro(request.getParameter("telPro"));
        employe.setVille(request.getParameter("ville"));

        return employe;
    }
    
    /**
     * 
     * @param con 
     */
    private void initUserList(DataAccess con){
        listUser = new ArrayList<>();
        try{
            ResultSet rs = con.getAllEmploye();
            while(rs.next()){
                listUser.add(resultSetLineToUtilisateur(rs));
            }
        } catch (SQLException e){

        }
    }
    
    /**
     * 
     * @param rs
     * @return
     * @throws SQLException 
     */
    private Employe resultSetLineToUtilisateur(ResultSet rs) throws SQLException {
        Employe employe = new Employe();
        employe.setAdresse(rs.getString("ADRESSE"));
        employe.setCodepostal(rs.getString("CP"));
        employe.setEmail(rs.getString("EMAIL"));
        employe.setId(Integer.parseInt(rs.getString("ID")));
        employe.setNom(rs.getString("NOM"));
        employe.setPrenom(rs.getString("PRENOM"));
        employe.setTeldom(rs.getString("TELDOM"));
        employe.setTelport(rs.getString("TELPORT"));
        employe.setTelpro(rs.getString("TELPRO"));
        employe.setVille(rs.getString("VILLE"));
        return employe;
    }
    
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    /**
     * 
     * @param login
     * @param mdp
     * @return 
     */
    private boolean checkCredentials(String login, String mdp) {
        try{
            DataAccess con = new DataAccess();
            Credentials credentials = new Credentials();
            credentials.setLogin(login);
            credentials.setMdp(mdp);       

            return con.checkCredentials(credentials).next();
        } catch (SQLException e){
            return false;
        }
    }

}
