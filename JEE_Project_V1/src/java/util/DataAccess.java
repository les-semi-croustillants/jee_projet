/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import pojo.Employe;
import util.constants.DBConstantes;
import util.constants.SQLConstantes;

/**
 *
 * @author jbi
 */
public class DataAccess {
    private String url;
    private String mdp;
    private String login;
    private Connection connection;
    
    public DataAccess(){
        try{
            this.url = PropertiesUtil.getDBProperty(DBConstantes.DB_URL);
            this.login = PropertiesUtil.getDBProperty(DBConstantes.DB_LOGIN);
            this.mdp = PropertiesUtil.getDBProperty(DBConstantes.DB_PASSWORD);
            Class.forName(PropertiesUtil.getDBProperty(DBConstantes.DB_DRIVER_CLASS)).newInstance();
            this.connection = DriverManager.getConnection(url, login, mdp);
        } catch (SQLException | ClassNotFoundException | InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }
    
    public ResultSet execQuery(String s){
        return execQueryWithParams(s, null);
    }
    
    public ResultSet execQueryWithParams(String s, List<String> params){
        try{
            PreparedStatement stat = preparedStatement(s, params);
            return stat.executeQuery();
        } catch (SQLException e) {
            String message = e.getMessage();
            e.printStackTrace();
            return null;
        }
    }
    
    public void execUpdate(String s, List<String> params){
        try{
            PreparedStatement stat = preparedStatement(s, params);
            stat.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    
    public ResultSet checkCredentials(Credentials credentials) {
        String query = PropertiesUtil.getSQLProperty(SQLConstantes.SQL_CHECK_AUTHENTIFICATION_QUERY);
        
        return execQueryWithParams(query, Arrays.asList(credentials.getLogin(), credentials.getMdp()));
    }

    public ResultSet getAllEmploye(){
        String query = PropertiesUtil.getSQLProperty(SQLConstantes.SQL_SELECT_ALL_EMPLOYES_QUERY);

        return execQuery(query);
    }
    
    public ResultSet getEmploye(Integer id){
        String query = PropertiesUtil.getSQLProperty(SQLConstantes.SQL_GET_EMPLOYE_QUERY);
        
        return execQueryWithParams(query, Arrays.asList(id.toString()));
    }
    
    public void addEmploye(Employe parameter) {
        String query = PropertiesUtil.getSQLProperty(SQLConstantes.SQL_INSERT_EMPLOYE_QUERY);

        execUpdate(query, fromEmployeToParamList(parameter));
    }
    
    public void deleteEmploye(Integer id) {
        String query = PropertiesUtil.getSQLProperty(SQLConstantes.SQL_DELETE_EMPLOYES_QUERY);

        execUpdate(query, Arrays.asList(id.toString()));
    }
    
    public void updateEmploye(Employe employe){
        Integer employeId = employe.getId();
        String query = PropertiesUtil.getSQLProperty(SQLConstantes.SQL_UPDATE_EMPLOYES_QUERY);
        List<String> params = fromEmployeToParamList(employe);
        params.add(employeId.toString());
        execUpdate(query, params);
    }
    
    private List<String> fromEmployeToParamList(Employe parameter){
        List<String> paramList = new ArrayList<>();
        paramList.addAll(Arrays.asList(parameter.getNom() 
                , parameter.getPrenom() 
                , parameter.getTeldom() 
                , parameter.getTelport() 
                , parameter.getTelpro() 
                , parameter.getAdresse() 
                , parameter.getVille()
                , parameter.getCodepostal() 
                , parameter.getEmail()));
        return paramList;
    }

    /**
     * @return the url
     */
    public String getUrl() {
        return url;
    }

    /**
     * @param url the url to set
     */
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * @return the mdp
     */
    public String getMdp() {
        return mdp;
    }

    /**
     * @param mdp the mdp to set
     */
    public void setMdp(String mdp) {
        this.mdp = mdp;
    }

    /**
     * @return the login
     */
    public String getLogin() {
        return login;
    }

    /**
     * @param login the login to set
     */
    public void setLogin(String login) {
        this.login = login;
    }

    /**
     * @return the connection
     */
    public Connection getConnection() {
        return connection;
    }

    /**
     * @param connection the connection to set
     */
    public void setConnection(Connection connection) {
        this.connection = connection;
    }

    private PreparedStatement preparedStatement(String s, List<String> params) throws SQLException {
        PreparedStatement stat = connection.prepareStatement(s);
        if(!CheckUtil.isEmptyList(params)){
            for(int i = 0; i < params.size() ; i++){
                stat.setString(i + 1, params.get(i));
            }
        }
        return stat;
    }
}
