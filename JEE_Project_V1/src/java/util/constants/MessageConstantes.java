/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util.constants;

/**
 *
 * @author jbi
 */
public enum MessageConstantes {
    MISSING_FIELD_ERROR_MESSAGE, AUTHENTIFICATION_ERROR_MESSAGE, EMPTY_LIST_ERROR_MESSAGE, DELETE_FAIL_ERROR_MESSAGE, INSERT_FAIL_ERROR_MESSAGE, 
    SESSION_ACTIVE_MESSAGE, 
    DELETE_SUCCESS_MESSAGE,
    MANDATORY_SELECTION_MESSAGE
}
