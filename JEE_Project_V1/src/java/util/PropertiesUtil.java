/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import util.constants.DBConstantes;
import util.constants.MessageConstantes;
import util.constants.SQLConstantes;
import util.constants.URIConstantes;

/**
 *
 * @author jbi
 */
public class PropertiesUtil {
    private final static String FILE = "properties/db.properties";
    
    /**
     * 
     * @param property
     * @return 
     */
    public static String getDBProperty(DBConstantes property){
        return getPropertyBody(property);
    }
    
    /**
     * 
     * @param property
     * @return 
     */
    public static String getErrorMessageProperty(MessageConstantes property){
        return getPropertyBody(property);
    }
    
    /**
     * 
     * @param property
     * @return 
     */
    public static String getSQLProperty(SQLConstantes property){
        return getPropertyBody(property);
    }
    
    /**
     * 
     * @param property
     * @return 
     */
    public static String getURIProperty(URIConstantes property){
        return getPropertyBody(property);
    }

    /**
     * 
     * @param property
     * @return 
     */
    private static String getPropertyBody(Enum property) {
        try{
            Properties prop = new Properties();
            InputStream in;
            ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
            in = classLoader.getResourceAsStream(FILE);
            prop.load(in);
            return prop.getProperty(property.name());
        } catch (IOException e){
            return null;
        }
    }

}
