/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import java.util.List;

/**
 *
 * @author jbi
 */
public class CheckUtil {
    
    /**
     * 
     * @param string
     * @return 
     */
    public static boolean isEmptyString(String string){
        return string == null || string.isEmpty();
    }

    /**
     * 
     * @param <T>
     * @param list
     * @return 
     */
    public static <T> boolean isEmptyList(List<T> list) {
        return list == null || list.isEmpty();
    }
}
